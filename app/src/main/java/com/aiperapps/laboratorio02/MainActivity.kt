package com.aiperapps.laboratorio02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnVerificar.setOnClickListener {

        when (edtNacimiento.text.toString().toInt()) {
                in 1930..1948 -> {
                    tvResultadoG.text = "Silent Generation"
                    imageView.setImageResource(R.drawable.silent_)
                 }
                in 1949..1968 -> {
                    tvResultadoG.text = "Baby Boom"
                    imageView.setImageResource(R.drawable.boo)
                 }
                in 1969..1980 -> {
                    tvResultadoG.text = "Generación X"
                    imageView.setImageResource(R.drawable.x)
                }
                in 1981..1993 -> {
                    tvResultadoG.text = "Generación Y"
                    imageView.setImageResource(R.drawable.y)
                }
                in 1994..2010 -> {
                    tvResultadoG.text = "Generación Z"
                    imageView.setImageResource(R.drawable.z)
                }
                else -> "No pertenece a ninguna generación"
            }

        }

    }
}